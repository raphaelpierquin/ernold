<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="lac" tilewidth="32" tileheight="32" tilecount="56" columns="8">
 <image source="images/lac.png" trans="000000" width="256" height="224"/>
 <wangsets>
  <wangset name="lac" type="corner" tile="-1">
   <wangcolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangtile tileid="8" wangid="0,0,0,1,0,0,0,0"/>
   <wangtile tileid="9" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="10" wangid="0,0,0,0,0,1,0,0"/>
   <wangtile tileid="16" wangid="0,1,0,1,0,0,0,0"/>
   <wangtile tileid="17" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="18" wangid="0,0,0,0,0,1,0,1"/>
   <wangtile tileid="24" wangid="0,1,0,0,0,0,0,0"/>
   <wangtile tileid="25" wangid="0,1,0,0,0,0,0,1"/>
   <wangtile tileid="26" wangid="0,0,0,0,0,0,0,1"/>
   <wangtile tileid="35" wangid="0,1,0,0,0,1,0,1"/>
   <wangtile tileid="36" wangid="0,1,0,1,0,0,0,1"/>
   <wangtile tileid="43" wangid="0,0,0,1,0,1,0,1"/>
   <wangtile tileid="44" wangid="0,1,0,1,0,1,0,0"/>
  </wangset>
 </wangsets>
</tileset>
